# 2.4.4

## Version 2.4.4 （2023/10/26）

发布功能

* 升级xterm到5.3.0
* 终端添加`重新连接`
* 终端自动切换到上次会话、目录从前端实现
* 忽略websocket主动关闭引发的异常
* 数据库 `执行SQL的查询` 添加 `最大展示行数` 默认 `500`
  * 指定前端最大数据展示行数，超出的数据将被忽略
  * 避免未控制好数量导致内存消耗过多
* 升级部分依赖库